<?php
require_once 'connection.php';
session_start();
$user = $_SESSION['user_login'];

if (isset($_REQUEST["btn_exit"])) {
  session_destroy();
  header("refresh:0.5,auth.php");
}

?>

<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.0/font/bootstrap-icons.css">
    <link rel="stylesheet" href="css/style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
            crossorigin="anonymous"></script>
    <script src="js/jquery.maskedinput.js"></script>

    <title>Агенство недвижимости</title>

</head>
<body>
  <header>
    <div class="container p-2">
      <div class="row align-items-center">
        <div class="col-10">
          <div class="header_logo"><img class="img-fluid" src="img/logo.png" alt=""></div>
        </div>
        <div class="col-1 fs-5 fw-bold text-end">
          <div class="user"><?echo $user;?></div>
        </div>
        <div class="col-1">
          <form>
            <input type="submit" class='btn' name='btn_exit' value="Выход">
          </form>
        </div>


      </div>
    </div>
  </header>
  <div class="container mt-3">
    <div class="row">
      <div class="col-3">
        <h4 class="text-white fw-bold">Агенство недвижимости</h4>
      </div>
      <div class="col-2">
        <button class="btn btn-primary addModalbtn d-none" data-bs-toggle="modal" data-bs-target="#addModal"><i class="bi bi-plus"></i> Добавить</button>
      </div>
        <div class="col-4 sort">
            Сортировать по: <strong> цене </strong>(<span id="pricea">возрастание</span> / <span id="priced">убывание</span>)
        </div>
    </div>
  </div>
  <div class="container object mt-3">
      <div class="fon row">

      </div>
      <div class="text-center">
          <div class="lds-dual-ring"></div>
      </div>
    <div id="object" class="row b-obj p-2 rounded pt-3 d-none">

    </div>
  </div>


  <div class="modal fade" id="addModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Добавление недвижимости</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Закрыть"></button>
        </div>
        <form method="POST" id="frmAdd" enctype="multipart/form-data">
          <div class="modal-body">
            <div class="mb-3">
              <label class="form-label">Наименование объекта</label>
              <input type="text" id="nameObj" name="titleObj" class="form-control">
            </div>
            <div class="mb-3">
              <label class="form-label">Адрес объекта</label>
              <input type="text" id="addressObj" name="addressObj" class="form-control">
            </div>
            <div class="mb-3">
              <label class="form-label">Описание объекта</label>
              <textarea name="descriptObj" id="descriptObj" class="form-control"></textarea>
            </div>
            <div class="mb-3">
              <label class="form-label">Цена объекта</label>
              <input type="text" id="priceObj" name="priceObj" class="form-control">
            </div>
            <div class="mb-3">
              <label class="form-label">Статус объекта</label>
              <select name="statusObj" id="statusObj" class="form-select">
                <option value="Актуально">Актуально</option>
                <option value="Не актуально">Не актуально</option>
              </select>
            </div>
            <div class="mb-3">
              <label class="form-label">Изображения объекта</label>
              <input type="file" name="imgObj" id="imgObj" accept="image/*" class="form-control" id="inputGroupFile04" aria-describedby="inputGroupFileAddon04" aria-label="Upload">
            </div>
          </div>
          <div class="alert alert-success addSucc d-none" ></div>
          <div class="modal-footer">
            <input type="submit" name="addObj" class="btn btn-primary" value="Добавить">
          </div>
        </form>
      </div>
    </div>
  </div>
  <script type="application/javascript" src="js/js.js"></script>
    <? if ($user =='admin'){?>
<script>
    $(document).ready(function (){
        $(".addModalbtn").removeClass('d-none');
    })

</script><?php }?>


</body>


</html>