<?php
require_once 'connection.php';
session_start();
$user = $_SESSION['user_login'];
if ($user =='admin'){
    $select_stmt = $db->query("SELECT * FROM object");
}
else{
    $select_stmt = $db->query("SELECT * FROM object WHERE statusObj = 'Актуально'");
}
try {

    $select_stmt;
    while ($row = $select_stmt->fetch()) {
        $title = $row["nameObj"];
        $addressObj = $row["addressObj"];
        $descriptionObj = $row["descriptionObj"];
        $priceObj = $row["priceObj"];
        $imgObj = $row["imgObj"];
        echo '<div class="col-3">';
        echo '<img class="img-fluid rounded" src="' . $imgObj . '">';
        echo '<h5 class="text-center text-white pt-3 fw-bold">' . $title . '</h5>';
        echo '<div class="text-white"><b>Адрес:</b> ' . $addressObj . '</div>';
        echo '<div class="text-white"><b>Описание:</b> ' . $descriptionObj . '</div>';
        echo '<div class="text-white fw-bold mt-3 fs-5 text-center">' . $priceObj . ' &#8381;</div>';
        echo '</div>';
    }
} catch (PDOException $e) {
    $e->getMessage();
}
?>
