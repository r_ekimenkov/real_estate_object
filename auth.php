<?php
require_once 'connection.php';
session_start();
if(isset($_SESSION["user_login"])){
  header("location: index.php");
}
if(isset($_REQUEST["btn_login"])){
  $userLogin = strip_tags($_REQUEST["txt_login"]);
  $password = strip_tags($_REQUEST["txt_password"]);
if(empty($userLogin)){
  $errorMsg[]="Введите логин";
}
else if(empty($password)){
  $errorMsg[]="Введите пароль";
}
else{
  try{
    $select_stmt=$db->prepare("SELECT * FROM users WHERE login=:ulogin AND password=:upassword");
    $select_stmt->execute(array(':ulogin'=>$userLogin,':upassword'=>$password));
    $row = $select_stmt->fetch(PDO::FETCH_ASSOC);
    if($select_stmt->rowCount()>0){
      $_SESSION["user_login"]=$row["login"];
      header("refresh:0.5,index.php");  
    }
    else{
      $errorMsg[]="Неверный логин или пароль";
    }
  }
  catch(PDOException $e){
    $e->getMessage();
  }
}
}
?>


<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
    <title>Авторизация</title>
</head>
<body>
    <div class="container h-100 d-flex">
        <div class="row w-100 align-items-center">
            <div class="col-6 p-5 mx-auto rounded auth_form">
                <h1 class="text-center mb-4">Авторизация</h1>
                <form method="POST">
                    <div class="mb-3">
                      <label for="" class="form-label">Логин:</label>
                      <input type="text" name="txt_login" class="form-control" id="exampleInputEmail1">
                    </div>
                    <div class="mb-3">
                      <label for="exampleInputPassword1" class="form-label">Пароль:</label>
                      <input type="password" name = "txt_password"class="form-control" id="exampleInputPassword1">
                    </div>
                    <?php
                    if(isset($errorMsg)){
                      foreach($errorMsg as $error){?>
                        <div class="alert alert-danger">
                          <strong><?php echo $error;?></strong>
                        </div><?php
                      }
                    }
                    ?>
                    <button type="submit" name="btn_login" class="btn btn-primary px-4 py-2 fs-5 rounded">Войти</button>
                  </form>
            </div>
        </div>
      </div>
</body>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</html>