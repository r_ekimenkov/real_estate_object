-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Мар 28 2022 г., 16:53
-- Версия сервера: 8.0.24
-- Версия PHP: 7.1.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `agency`
--

-- --------------------------------------------------------

--
-- Структура таблицы `object`
--

CREATE TABLE `object` (
  `id` int NOT NULL,
  `nameObj` varchar(255) NOT NULL,
  `addressObj` varchar(255) NOT NULL,
  `descriptionObj` text NOT NULL,
  `priceObj` varchar(255) NOT NULL,
  `statusObj` varchar(255) NOT NULL,
  `imgObj` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `object`
--

INSERT INTO `object` (`id`, `nameObj`, `addressObj`, `descriptionObj`, `priceObj`, `statusObj`, `imgObj`) VALUES
(8, 'Объект 1', 'ул. Мамадышская 45-78', '9-ый дом, 180 кв. метров, двухуровневая', '7500000', 'Актуально', 'img/dom-gorodishce-141072644-2.jpg'),
(9, 'Объект 2', 'ул. Гагарина 56-56', 'ул. Азата Аббасова, д. 11, Казань', '3900000', 'Актуально', 'img/1944656.jpg'),
(10, 'Объект 10', ' ул. Фучика 6-97', 'Продается 2-комн. кв., 62.8 м2, 2/16 этаж', '5200000', 'Актуально', 'img/1944656.jpg'),
(11, 'Объект 9', 'ул. Хо Ши Мина  56-321', 'Продается 4-комн. кв., 83.5 м2, 10/11 этаж', '3750000', 'Актуально', 'img/1930518.jpg'),
(20, 'dsadasd', 'asdasdas', 'asdasdasd', '1231231', 'Актуально', 'img/1930518.jpg'),
(21, 'sdfsdfs', 'sdfsdf', 'sdfasdfsdf', '1', '', 'img/1930518.jpg');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int NOT NULL,
  `login` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `login`, `password`, `role`) VALUES
(1, 'admin', 'admin', 'Rieltor');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `object`
--
ALTER TABLE `object`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `object`
--
ALTER TABLE `object`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
